// ------------- JQUERY PLUGIN -----------------------
(function ( $ ) {

    $.fn.popItUp = function(options) {


        var settings = $.extend({
            wrapper: "#dialog",
            confirm: "#okbutton",
            cancel: "#cancelbutton",
            callbacks: {okaction:function(){alert("OK BUTTON!")},closeaction:function(){alert("CANCEL BUTTON!")}},
            animtype: "slide"
        }, options );

        $.fn.popItUp.open=function(){
            $wrapper = $(settings.wrapper);
            if(settings.animtype=="slide"){
                $wrapper.slideDown(2000);
            }else if(settings.animtype=="fade"){
                $wrapper.fadeIn(2000);
            }
        }

        function actionAndClose(action){
            action()
            $wrapper = $(settings.wrapper);
            if(settings.animtype=="slide"){
                $wrapper.slideUp(1500);
            }else if(settings.animtype=="fade"){
                $wrapper.fadeOut(1500);
            }
        }


        $(this).click($.fn.popItUp.open)
        $confirm = $(settings.confirm)
        $cancel = $(settings.cancel)

        $confirm.click(()=>actionAndClose(settings.callbacks.okaction))
        $cancel.click(()=>actionAndClose(settings.callbacks.closeaction))
        return this;
    };

}( jQuery ));