﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAppCore.Models
{
    public class MockPizzaManager : IPizzaManager
    {
        private List<Pizza> pizze { get; set; }

        public MockPizzaManager()
        {

            pizze = new List<Pizza>();
            pizze.Add(new Pizza(0,"MargeritaTest","Pomodoro, Mozzarella"));
            pizze.Add(new Pizza(1,"Capricciosa", "salsa di pomodoro,mozzarella,prosciutto cotto,funghi,olive,carciofini"));
            pizze.Add(new Pizza(2,"Marinara","Pomodoro, Aglio"));
            pizze.Add(new Pizza(3,"Napoli","Pomodoro,Capperi,Acciughe"));

        }

        public List<Pizza> getPizze()
        {
            return pizze;
        }

        public Pizza getPizzaById(int id)
        {
            foreach(Pizza p in pizze)
            {
                if(p.Id == id)
                {
                    return p;
                }
            }
            return null;
        }

        public void setPizzaById(int id, string name, string description)
        {
            foreach (Pizza p in pizze)
            {
                if (p.Id == id)
                {
                    p.Name = name;
                    p.Description = description;
                }
            }
            return;
        }

        public void deletePizza(Pizza p)
        {
            pizze.Remove(p);
            for (int i = 0; i < pizze.Count; i++)
            {
                pizze[i].Id = i;
            }
        }

        public void addPizza(string name, string description)
        {
            Pizza p = new Pizza(pizze.Count, name, description);
            pizze.Add(p);
        }

        public List<Pizza> getSet(int start, int count)
        {
            var curr_count = pizze.Count-start;
            if (curr_count > count)
            {
                curr_count = count;
            }
            return pizze.GetRange(start,curr_count);
        }
    }
}
