﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using MySqlConnector;
using System.Data.Common;

namespace TestAppCore.Models
{
    public class SQLPizzaManager : IPizzaManager
    {
        private static string CONN_STR = "server=127.0.0.1;user=root;database=test";

        private DbConnection getConnection()
        {
            return new MySqlConnection(CONN_STR);
        }

        public void addPizza(string name, string description)
        {
            if (name != null && description != null)
            {
                var query = $"INSERT INTO pizzatable (id,name,description) VALUES (NULL,@name,@description)";
                using (var connection = getConnection())
                {
                    connection.Execute(query, new { name, description });
                }
            }
        }

        public void deletePizza(Pizza p)
        {
            if (p != null)
            {
                var query = $"DELETE FROM pizzatable WHERE name = @name AND description=@description AND id=@id";
                using (var connection = getConnection())
                {
                    connection.Execute(query, new { name = p.Name, description = p.Description, id = p.Id });
                }
            }
        }

        public Pizza getPizzaById(int id)
        {
            var query = $"SELECT * FROM pizzatable WHERE id=@id";
            using (var connection = getConnection())
            {
                var resp = connection.QueryFirst<Pizza>(query, new { id});
                if(resp is null) { return null; }
                return (Pizza)resp;
            }
        }

        public List<Pizza> getPizze()
        {
            var query = $"SELECT id,name,description FROM pizzatable;";
            using(var connection= getConnection())
            {
                var resp = connection.Query<Pizza>(query);
                return (List<Pizza>)resp;
            }
        }

        public List<Pizza> getSet(int start, int count)
        {
            var query = $"CALL selectBatch(@start,@count);";
            List<Pizza> ps;
            using (var connection = getConnection())
            {
                var resp = connection.Query<Pizza>(query, new { start,count });
                ps = (List<Pizza>)resp;
            }
            return ps;
        }

        public void setPizzaById(int id, string name, string description)
        {
            if (name != null && description != null)
            {
                var query = $"UPDATE pizzatable SET name = @name, description = @description WHERE id = @id;";
                using (var connection = getConnection())
                {
                    connection.Execute(query, new { name, description, id });
                }
            }
        }
    }
}
