﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAppCore.Models
{
    public class Order
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Pizza { get; set; }

        public Order(string name, string surname, string pizza)
        {
            Name = name;
            Surname = surname;
            Pizza = pizza;
        }
    }
}