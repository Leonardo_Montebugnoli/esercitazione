﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAppCore.Models
{
    public interface IPizzaManager
    {
        public List<Pizza> getPizze();
        public Pizza getPizzaById(int id);
        public void setPizzaById(int id, string name, string description);
        public void deletePizza(Pizza p);
        public void addPizza(string name, string description);
        List<Pizza> getSet(int start, int count);
    }
}
