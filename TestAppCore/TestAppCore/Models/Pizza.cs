﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAppCore.Models
{
    public class Pizza
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        

        public Pizza(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }

        public Pizza()
        {
            Id = -1;
            Name = "Default";
            Description = "Default";
        }


    }
}
