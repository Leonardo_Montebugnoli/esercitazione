// ------------- JQUERY PLUGIN -----------------------
(function ( $ ) {

    $.fn.popItUp = function (options) {


        var settings = $.extend({
            wrapper: "#dialog",
            confirm: "#okbutton",
            cancel: "#cancelbutton",
            callbacks: { okaction: function () { alert("OK BUTTON!") }, closeaction: function () { alert("CANCEL BUTTON!") } },
            animtype: "slide",
            anispeed: {
                in: "fast",
                out: "fast"
            }
        }, options);

        function open() {
            $wrapper = $(settings.wrapper);
            if (settings.animtype == "slide") {
                $wrapper.slideDown(settings.anispeed.in);
            } else if (settings.animtype == "fade") {
                $wrapper.fadeIn(settings.anispeed.in);
            }
        }

        function actionAndClose(action) {
            action()
            $wrapper = $(settings.wrapper);
            if (settings.animtype == "slide") {
                $wrapper.slideUp(settings.anispeed.out);
            } else if (settings.animtype == "fade") {
                $wrapper.fadeOut(settings.anispeed.out);
            }
        }


        open()
        $confirm = $(settings.confirm)
        $cancel = $(settings.cancel)



        $confirm.on("click", (e) => { e.preventDefault(); actionAndClose(settings.callbacks.okaction); })
        $cancel.on("click", (e) => { e.preventDefault(); actionAndClose(settings.callbacks.closeaction);})
        return this;
    };

}( jQuery ));