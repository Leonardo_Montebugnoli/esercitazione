﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestAppCore.Models;

namespace TestAppCore.Controllers
{
    public class OrderController : Controller
    {

        private IPizzaManager _manager;

        public OrderController(IPizzaManager manager)
        {
            _manager = manager;
        }

        [HttpPost]
        public IActionResult OrderNow(string inputName, string inputSurname, string pizzaSelect)
        {
            var order = new Order(inputName,inputSurname,pizzaSelect);
            ViewBag.manager = _manager;
            return View(order);
        }
    }
}
