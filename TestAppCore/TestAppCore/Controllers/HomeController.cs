﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TestAppCore.Models;

namespace TestAppCore.Controllers
{
    public class HomeController : Controller
    {
        private IPizzaManager _manager;

        public HomeController(IPizzaManager manager)
        {
            this._manager = manager;
        }

        public IActionResult Index()
        {
            ViewBag.manager = _manager;
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
