﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestAppCore.Models;

namespace TestAppCore.Controllers
{
    public class LoginController : Controller
    {
        private IPizzaManager _manager;

        public LoginController(IPizzaManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public IActionResult login()
        {
            return View("login");
        }

        [HttpPost]
        public IActionResult login(string inputUsername, string inputPassword)
        {
            ViewBag.manager = _manager;
            if (inputUsername.Equals("admin") && inputPassword.Equals("admin"))
            {
                ViewData["logged"] = true;
                return View("loginSuccess");
            }
            ViewData["logged"] = false;
            return View("loginFailed");
        }

        
        public IActionResult pizzaManager()
        {

            ViewBag.manager = _manager;
            return View("pizzamanager");
        }
        
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "AABCDEEFGHIIJKLMNOOPQRSTUUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public IActionResult SimpleTable( TableInput option)
        {
            var count = _manager.getPizze().Count;

            List<Object[]> result = new List<object[]>();
            int max = option.length <= 10 ? 10 : option.length;
            max = max > count ? count : max;
            List<Pizza> ps = _manager.getSet(option.start, max);
            foreach (Pizza p in ps)
            {
                result.Add(new Object[] { p.Name, p.Description, p.Id });
            }

            return Json(new { draw = option.draw, data = result, recordsTotal = count, recordsFiltered = count });
        }

        [HttpPost]
        public IActionResult SimpleAjax( CRUD_ACTION m, Pizza model)
        {
           
            if (m != CRUD_ACTION.delete && !ModelState.IsValid)
            {
                string[] errors = ModelState.Values.SelectMany(v => v.Errors).Select(v => v.ErrorMessage).ToArray();
                return Json(new { errors = errors });
            }

            
            JsonResult result = Json("OK");
            switch (m)
            {
                case CRUD_ACTION.create:
                    //@TODO create
                    _manager.addPizza(model.Name,model.Description);
                    break;

                case CRUD_ACTION.update:
                    //@TODO update
                    _manager.setPizzaById(model.Id, model.Name, model.Description);
                    break;
                case CRUD_ACTION.delete:
                    //@TODO delete
                    _manager.deletePizza(_manager.getPizzaById(model.Id));
                    break;
                default:
                    return Json(new { errors = new string[] { "Operazione non permessa" } });
            }
            return result;
        }
    }
}
